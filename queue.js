let a = [];

function print() {
	return a;
}

function enqueue(x) {
	a[a.length] = x;
	return print();
}

function dequeue() {
	for (let i = 0 ; i < a.length-1 ; i++)
		a[i] = a[i+1];
	a.length--;
	return print();
}

/*function dequeue() {  this works but di pwede, sayang.
	[a, ...y] = a;
	return a = y;
}*/

function front() {
	return a[0];
}

function size() {
	return a.length;
}

function isEmpty() {
	return (a.length == 0) ? true : false;
}

module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};